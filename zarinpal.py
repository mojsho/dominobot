# -*- coding: utf-8 -*-

# Sample Flask ZarinPal WebGate with SOAP

__author__ = 'Mohammad Reza Kamalifard, Hamid Feizabadi'
__url__ = 'reyhoonsoft.ir , rsdn.ir'
__license__ = "GPL 2.0 http://www.gnu.org/licenses/gpl-2.0.html"

from flask import Flask, url_for, redirect, request
from suds.client import Client

from message_handler import send_success_pay, send_failed_pay

app = Flask(__name__)

MMERCHANT_ID = 'ac51ebd4-f7f1-11e7-a1a4-005056a205be'  # Required
ZARINPAL_WEBSERVICE = 'https://www.zarinpal.com/pg/services/WebGate/wsdl'  # Required
description = "Domino Energy"  # Required
email = 'user@userurl.ir'  # Optional
mobile = '09123456789'  # Optional

global pay_amount
pay_amount = 0

global pay_chat_id
pay_chat_id = 0

global pay_message_id
pay_message_id = 0


@app.route('/request/<amount>/<chat_id>/<message_id>')
def send_request(amount, chat_id, message_id):
    global pay_amount, pay_chat_id, pay_message_id
    pay_amount = amount
    pay_chat_id = chat_id
    pay_message_id = message_id

    client = Client(ZARINPAL_WEBSERVICE)
    result = client.service.PaymentRequest(MMERCHANT_ID,
                                           amount,
                                           description,
                                           "sh.mojtaba94@gmail.com",
                                           "09396790309",
                                           str(url_for('verify', _external=True)))
    if result.Status == 100:
        return redirect('https://www.zarinpal.com/pg/StartPay/' + result.Authority)
    else:
        return 'Error'


@app.route('/verify/', methods=['GET', 'POST'])
def verify():
    global pay_amount, pay_chat_id, pay_message_id
    client = Client(ZARINPAL_WEBSERVICE)
    if request.args.get('Status') == 'OK':
        result = client.service.PaymentVerification(MMERCHANT_ID,
                                                    request.args['Authority'],
                                                    pay_amount)
        if result.Status == 100:
            send_success_pay(pay_chat_id, pay_message_id)
            return redirect("https://t.me/dominoenergybot")
            # return 'Transaction success. RefID: ' + str(result.RefID)
        elif result.Status == 101:
            send_success_pay(pay_chat_id, pay_message_id)
            return redirect("https://t.me/dominoenergybot")
            # return 'Transaction submitted : ' + str(result.Status)
        else:
            send_failed_pay(pay_chat_id, pay_message_id)
            return redirect("https://t.me/dominoenergybot")
            # return 'Transaction failed. Status: ' + str(result.Status)
    else:
        send_failed_pay(pay_chat_id, pay_message_id)
        return redirect("https://t.me/dominoenergybot")
        # return 'Transaction failed or canceled by user'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8004, debug=True)
